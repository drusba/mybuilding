<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $fillable = ['parent_id', 'name', 'description','avatar'];

    public function children()//if we need in the future children categories
    {
        return $this->hasMany('App\Category', 'parent_id');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\Building');
    }
}


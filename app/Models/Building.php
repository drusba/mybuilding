<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    use HasFactory;
    protected $fillable = ['name','description','address','active','user_id'];

    public function amenities()
    {
        return $this->hasMany('App\Models\Amenity');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category')->withTimestamps();
    }

    public function images()
    {
        return $this->hasMany('App\Models\BuildingImage');
    }
    public function floors()
    {
        return $this->hasMany('App\Models\Floor');
    }
    public function apartments()
    {
        return $this->hasMany('App\Models\Apartment');
    }
}
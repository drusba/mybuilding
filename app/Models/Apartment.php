<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Apartment extends Model
{
    use HasFactory;
    protected $fillable = ['apartment_number', 'type_of_apartment', 'floor_id','building_id'];

    public function rooms()
    {
        return $this->hasMany('App\Models\Room');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Floor extends Model
{
    use HasFactory;
    protected $fillable = ['floor_number', 'building_id'];

    public function apartments()
    {
        return $this->hasMany('App\Models\Apartment','floor_id','id');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Validator;
use Spatie\Permission\Models\Role;
use Intervention\Image\ImageManagerStatic as Image;


class GeneralController extends Controller
{
    public function upload_file(Request $request)
    {
        $user = auth()->user();
        $current_model = $this->checkModel($request['id'],$request['model_name']);
        Storage::disk('public_images')->delete($request['type'].$request['id'].'/'.$current_model->avatar);
        preg_match('/^data:image\/(\w+);base64,/', $request['file']);
        $image = substr($request['file'], strpos($request['file'], ',') + 1);
        $image = base64_decode($image);
        $image_name = $request['image_name'];
        $image = Image::make($image)->resize(300, 300, function ($constraint) {
            $constraint->aspectRatio();
        })->encode('jpg');
        Storage::disk('public_images')->put($request['type'].$request['id'].'/'.$image_name, $image);
        return $this->saveImage($image_name,$request['id'],$request['model_name']);

    }

    public function create_role_permission(Request $request)
    {
       $request->validate([
            'name' => 'required|string'
       ]);
       $role = Role::create(['name' => $request->name]);
       if($role){
           return response()->json( $role, 200);
       }
    }

    public function get_all_public_categories()
    {
        $category = Category::all();
        return response()->json($category, 200);
    }

    private function saveImage($file_name,$id,$model_name)
    {
        $model = $this->checkModel($id,$model_name);
        $model->avatar = $file_name;
        if($model->update()){
            return response()->json( $model, 200);
        }
    }

    private function checkModel($id,$model_name){
        switch ($model_name) {
            case 'user':
                $model = User::find($id);
                return $model;
                break;
            case 'category':
                $model = Category::find($id);
                return $model;
                break;
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function users(Request $request)
    {
        $users = User::with('roles')->paginate(5);
        return response()->json($users,200);
    }

    public function roles(){
        $roles = \Spatie\Permission\Models\Role::all();
        return response()->json($roles,200);
    }

    public function update_role(Request $request){
        $user = User::find($request['id']);
        $user->roles()->detach();
        $user->assignRole($request['role']);
        return response()->json($user,200);
    }

    public function update_user_status(Request $request){
        $user = User::find($request['id'])->update(['status'=> $request['check']]);
        $status = ($request['check'] == 0) ? 'disabled' : 'enabled';

        if($user){
            return response()->json('User status '.$status.' correctly',201);
        }else{
            return response()->json('An error occurred, please try again',201);
        }
    }

}

<?php

namespace App\Http\Controllers;

use App\Models\Floor;
use Illuminate\Http\Request;

class FloorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $floors = Floor::where(['building_id'=>$request['building_id']])->with('apartments')->paginate(5);
        return response()->json($floors);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'floors_number'=> 'required|integer|max:100',
            'building_id' => 'required|integer',
        ]);
        $found_floors = Floor::where('building_id',(int)$request->building_id)->count();
        if($found_floors > 0){
            $start_floor = $found_floors;
            $max_floor_create = $request['floors_number']+$start_floor;
        }else{
            $start_floor = 0;
            $max_floor_create = $request['floors_number'];
        }
        $floors = [];
        for ($start_floor; $start_floor < $max_floor_create ; $start_floor++) {
            $floor = Floor::create([
                'floor_number' => $start_floor+1,
                'building_id' => (int)$request->building_id,
            ]);
            array_push($floors,$floor);
        }
            return response()->json($floors,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Floor  $floor
     * @return \Illuminate\Http\Response
     */
    public function show(Floor $floor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Floor  $floor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $floor_id)
    {
        $request->validate([
            'floor_number' => 'required|integer|max:100',
        ]);
        $floor = Floor::find($floor_id)->update($request->all());
        return response()->json($floor,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Floor  $floor
     * @return \Illuminate\Http\Response
     */
    public function destroy($floor_id)
    {
        $floor = Floor::find($floor_id);
        $floor->apartments()->delete();
        if($floor->delete()){
            return response('The floor and its apartments were erased.',200);
        };
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Apartment;
use Illuminate\Http\Request;

class ApartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $apartments = Apartment::where(['building_id'=>$request['building_id'],'floor_id'=>$request['floor_id']])->with('rooms')->paginate(5);
        return response()->json($apartments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'apartment_number'=>'required|string',
            'type_of_apartment'=>'required|string',
            'floor_id' => 'required|integer',
            'building_id' => 'required|integer'
        ]);
        $apartment = Apartment::create([
            'apartment_number' => $request->apartment_number,
            'type_of_apartment' => $request->type_of_apartment,
            'floor_id' => $request->floor_id,
            'building_id' => $request->building_id,
        ]);
        return response()->json($apartment,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Apartment  $apartament
     * @return \Illuminate\Http\Response
     */
    public function show(Apartment $apartament)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Apartment  $apartament
     * @return \Illuminate\Http\Response
     */
    public function edit(Apartment $apartament)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Apartment  $apartament
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $apartment_id)
    {
        $request->validate([
            'apartment_number'=>'required|string',
            'type_of_apartment'=>'required|string',
            'floor_id' => 'required|integer',
            'building_id' => 'required|integer'
        ]);
        $apartment = Floor::find($apartment_id)->update($request->all());
        return response()->json($apartment,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Apartment  $apartament
     * @return \Illuminate\Http\Response
     */
    public function destroy($apartament_id)
    {
        $apartament = Apartment::find($apartament_id);
        if($apartament->delete()){
            return response('Apartment and rooms were deleted',200);
        };
    }
}

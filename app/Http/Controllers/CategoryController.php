<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //if we need in the future with children relationship
        //$categories = Category::with('children')->whereNull('parent_id')->orderBy('name', 'ASC')->paginate(10);
        $categories = Category::paginate(5);
        return response()->json($categories,200);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string|max:125',
            'description' => 'string|max:1500',
            'avatar' => 'required|string',
            'parent_id' => ''
        ]);

        $category = Category::create($data);
        return response()->json($category,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$category_id)
    {
        $request->validate([
            'name' => 'required|string',
            'description' => 'string',
        ]);
        $category = Category::find($category_id)->update($request->all());
        return response('Category was updated',200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($category_id)
    {
        $category = Category::find($category_id);
        if($category->delete()){
            Storage::disk('public_images')->delete('admin/category/'.$category_id.'/'.$category->avatar);
            return response('Category was deleted',200);
        };
    }

    public function all_categories()
    {
        $categories = Category::select('id', 'name')->get();;
        return response()->json($categories,200);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class AuthController extends Controller
{
    use SendsPasswordResetEmails;

    public function login(Request $request)
    {
            $response = Http::asForm()->post(config('services.passport.login_endpoint'),[
                'grant_type' => 'password',
                'client_id' => config('services.passport.client_id'),
                'client_secret' => config('services.passport.client_secret'),
                'username' => $request->username,
                'password' => $request->password,
            ]);
            $response->throw();
            ;
            return $response->getBody();
    }

    public function register(Request $request)
    {
        $request->validate([
            'name'=>['required', 'string', 'max:60'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        if($user){
            $user_role_create = User::find($user->id);
            $user_role_create->assignRole('user');
            return response()->json($user,200);
        }

    }

    public function logout()
    {
        auth()->user()->tokens->each(function ($token,$key){
            $token->delete();
        });
        return response()->json('Logged out successfully',200);
    }

    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'name' => 'required|string',

        ]);
        $user = User::find($id);
        $user->update($data);

        if($user){
            return response()->json($user,200);

        }else{
            return response()->json('An error occurred, please try again',400);
        }
    }
    /**Password reset request**/
    public function sendPasswordResetLink(Request $request)
    {
        return $this->sendResetLinkEmail($request);
    }

    protected function sendResetLinkResponse(Request $request, $response)
    {
        return response()->json([
            'message' => 'Password change email sent.',
            'data' => $response
        ]);
    }

    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return response()->json(['message' => 'Email could not be sent to this email address.']);
    }

    public function callResetPassword(Request $request)
    {
        return $this->reset($request);
    }

    protected function sendResetResponse(Request $request, $response)
    {
        return response()->json(['message' => 'Password reset successfully.']);
    }

    protected function sendResetFailedResponse(Request $request, $response)
    {
        return response()->json(['message' => 'Failed, Invalid Token.']);
    }
}

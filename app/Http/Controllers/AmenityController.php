<?php

namespace App\Http\Controllers;

use App\Models\Amenity;
use App\Models\Building;
use Illuminate\Http\Request;

class AmenityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $amenities = Amenity::where(['building_id'=>$request['building_id'], 'active' => true])->paginate(5);
        return $amenities;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required|string|max:255',
            'description' => 'required|string',
            'code' => 'required|string',
            'open_at' => 'required|string',
            'close_at' => 'required|string',
            'building_id' => 'required|integer'
        ]);

        $amenity = Amenity::create([
            'name' => $request->name,
            'description' => $request->description,
            'code' => $request->code,
            'open_at' => $request->open_at,
            'close_at' => $request->close_at,
            'status' => 'available',
            'active' => true,
            'building_id' => $request->building_id,
        ]);
        return response()->json($amenity,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Amenity  $amenity
     * @return \Illuminate\Http\Response
     */
    public function show(Amenity $amenity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Amenity  $amenity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $amenity_id)
    {
        $request->validate([
            'name'=>'required|string|max:255',
            'description' => 'required|string',
            'code' => 'required|string',
            'open_at' => 'required|string',
            'close_at' => 'required|string',
        ]);

        $amenity = Amenity::find($amenity_id)->update($request->all());
        return response()->json($amenity,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Amenity  $amenity
     * @return \Illuminate\Http\Response
     */
    public function destroy($amenity_id)
    {
        $amenity = Amenity::find($amenity_id);
        if($amenity->delete()){
            return response('Amenity was deleted',200);
        };
    }
    /**
    * Update status of amenity
     **/
    public function update_status(Request $request){
        $amenity = Amenity::find($request['amenity_id']);
        if($amenity){
            $amenity->status = $request['status'];
            if($amenity->save()){
                return response('Amenity status was updated successfully',200);
            };
        }
    }
    /**
     * Update active field of amenity
     **/
    public function update_active(Request $request){
        $amenity = Amenity::find($request['amenity_id']);
        if($amenity){
            $amenity->active = $request['active'];
            if($amenity->save()){
                return response('Amenity active was updated successfully',200);
            };
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Building;
use App\Models\BuildingImage;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class BuildingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $user_buildings = Building::where(['user_id'=>$user->id,'active'=>true])->with('images')->with('categories')->paginate(5);
        return response()->json($user_buildings,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = auth()->user();
        $request->validate([
            'name' => 'required|string|max:255',
            'description' => 'string',
            'address' => 'required|string',
            'categories' => '',
            'images' => 'required|array|min:1|max:5'
        ]);

        $building = Building::create([
            'name' => $request->name,
            'description' => $request->description,
            'address' => $request->address,
            'active' => true,
            'user_id' => $user->id,
        ]);

        $this->save_building_categories($request['categories'],$building->id);
        $this->save_building_images($request['images'],$building->id);
        return response()->json($building,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Building  $building
     * @return \Illuminate\Http\Response
     */
    public function show(Building $building)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Building  $building
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $building_id)
    {
        $request->validate([
            'name' => 'required|string',
            'description' => 'string|max:2500',
            'address' => 'required|string',
            'categories' => '',
            'images' => 'max:5'
        ]);
        $this->update_building_categories($request['categories'],$building_id);
        is_array($request['images']) ? $this->save_building_images($request['images'],$building_id) : '';
        $building = Building::find($building_id)->update($request->all());
        return response()->json($building,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Building  $building
     * @return \Illuminate\Http\Response
     */
    public function destroy($building_id)
    {
        $build = Building::find($building_id);
        $build->amenities()->delete();
        $build->images()->delete();
        $build->apartments()->delete();
        $build->floors()->delete();
        if($build->delete()){
            return response('Building with amenities were deleted',200);
        };
    }
    public function destroy_image($image_building_id)
    {
        $image = BuildingImage::where('id',$image_building_id)->first();
        $count_images = BuildingImage::where('building_id',$image['building_id'])->count();
        if($count_images >= 2){
            if($image->delete()){
                Storage::disk('public_images')->delete('building/'.$image->building_id.'/'.$image->name);
                return response('Building image removed',200);
            }
        }else{
            return response('You can\'t delete the last image',409);
        }
    }

    /**
     * Update active field of active
     **/
    public function update_active(Request $request)
    {
        $building = Building::find($request['building_id']);
        if($building){
            $building->active = $request['active'];
            if($building->save()){
                return response('Building active was updated successfully',200);
            };
        }
    }

    /**
     * Get all public buildings
     **/
    public function public_buildings(Request $request)
    {
        $buildings = Building::with('amenities')
            ->with('images')
            ->with('categories')
            ->with('floors')
            ->with('apartments')
            ->limit(6)->get();
        return response()->json($buildings,200);
    }

    /**
     * Get all public buildings
     **/
    public function search_buildings(Request $request)
    {
        $search_word = (isset($request['filters']['search'])) ? $request['filters']['search']: '';
        $category = (isset($request['filters']['category'])) ? array_column($request['filters']['category'], 'name') : [];
        $count_floors = (isset($request['filters']['floors'])) ? $request['filters']['floors'] : 0;
        $count_apartments = (isset($request['filters']['apartments'])) ? $request['filters']['apartments'] : 0;
        $buildings = Building::when($search_word, function ($q) use ($search_word) {
            return $q->where('name', 'like', '%' . $search_word . '%');
            })->whereHas("categories", function ($query) use ($category) {
                $query->whereIn("name", $category);
            }, "=", count($category))
            ->has('floors', '>=', $count_floors)
            ->has('apartments', '>=', $count_apartments)
            ->with('amenities')
            ->with('floors')
            ->with('apartments')
            ->with('images')
            ->with('categories')
            ->paginate(4);
        return response()->json($buildings,200);
    }

    public function all_buildings(Request $request)
    {
        $buildings = Building::with('amenities')
            ->with('images')
            ->with('categories')
            ->with('floors')
            ->with('apartments')
            ->paginate(4);
        return response()->json($buildings,200);
    }
    /**
     * Save building categories
     **/
    private function save_building_categories($categories,$building_id)
    {
        $building = Building::find($building_id);
        foreach ($categories as $category){
            $building->categories()->attach($category['id']);
        }
    }
    /**
     * Update building categories
     **/
    private function update_building_categories($categories,$building_id)
    {
        $building = Building::find($building_id);
        $building->categories()->where('building_id',$building_id )->detach();
        $new_arrays = array_column($categories, 'id');
        foreach ($new_arrays as $cat){
            $building->categories()->attach($cat);
        }
    }
    /**
     * Save building images
     **/
    private function save_building_images($images,$building_id)
    {
        foreach ($images as $img){
            preg_match('/^data:image\/(\w+);base64,/', $img['image']);
            $image = substr($img['image'], strpos($img['image'], ',') + 1);
            $image = base64_decode($image);
            $image_name = $img['image_name'];
            $image = Image::make($image)->resize(400, null, function ($constraint) {
                        $constraint->aspectRatio();
                     })->encode('jpg');
            Storage::disk('public_images')->put('building/'.$building_id.'/'.$image_name, $image);
            $building_image = BuildingImage::create([
                'name' => $image_name,
                'url' => 'building/'.$building_id.'/'.$image_name,
                'building_id' => $building_id,
            ]);
        }
    }
}

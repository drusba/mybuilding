<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*User*/

Route::post('/login',[\App\Http\Controllers\AuthController::class, 'login']);
Route::post('/register',[\App\Http\Controllers\AuthController::class, 'register']);
Route::middleware('auth:api')->post('/logout',[\App\Http\Controllers\AuthController::class, 'logout']);
Route::middleware('auth:api')->put('/user/update/{user_id}',[\App\Http\Controllers\AuthController::class, 'update']);
/*************/

/*Reset Password*/
Route::post('/password_email', [\App\Http\Controllers\AuthController::class, 'sendPasswordResetLink']);
Route::post('/password_reset', [\App\Http\Controllers\ResetPasswordController::class, 'callResetPassword']);
/*************/

/*Public Buildings*/
Route::get('/public-buildings', [\App\Http\Controllers\BuildingController::class, 'public_buildings']);
Route::post('/search-buildings', [\App\Http\Controllers\BuildingController::class, 'search_buildings']);
Route::post('/all-buildings', [\App\Http\Controllers\BuildingController::class, 'all_buildings']);
/*************/
/*Public categories*/
Route::get('/all_public_categories', [\App\Http\Controllers\GeneralController::class, 'get_all_public_categories']);
/************/
Route::middleware('auth:api')->group(function(){
    Route::get('/user', function (Request $request) {
        return ['user'=>$request->user(),'roles'=>$request->user()->getRoleNames()];
    });
    /*Global*/
    Route::post('/upload_file', [\App\Http\Controllers\GeneralController::class, 'upload_file']);
    /*************/
    Route::group(['middleware' => ['role:user']], function () {
        /*Buildings*/
        Route::get('/buildings', [\App\Http\Controllers\BuildingController::class, 'index']);
        Route::post('/new_building', [\App\Http\Controllers\BuildingController::class, 'store']);
        Route::post('/active_building', [\App\Http\Controllers\AmenityController::class, 'update_active']);
        Route::put('/update_building/{building_id}', [\App\Http\Controllers\BuildingController::class, 'update']);
        Route::delete('/remove_building/{building_id}', [\App\Http\Controllers\BuildingController::class, 'destroy']);
        Route::delete('/remove_image_building/{image_building_id}', [\App\Http\Controllers\BuildingController::class, 'destroy_image']);
        /*************/
        /*Amenities*/
        Route::get('/amenities', [\App\Http\Controllers\AmenityController::class, 'index']);
        Route::post('/new_amenity', [\App\Http\Controllers\AmenityController::class, 'store']);
        Route::post('/status_amenity', [\App\Http\Controllers\AmenityController::class, 'update_status']);
        Route::post('/active_amenity', [\App\Http\Controllers\AmenityController::class, 'update_active']);
        Route::put('/update_amenity/{amenity_id}', [\App\Http\Controllers\AmenityController::class, 'update']);
        Route::delete('/remove_amenity/{amenity_id}', [\App\Http\Controllers\AmenityController::class, 'destroy']);
        /*************/
        /*Floors*/
        Route::get('/building_floors', [\App\Http\Controllers\FloorController::class, 'index']);
        Route::post('/new_floors', [\App\Http\Controllers\FloorController::class, 'store']);
        Route::put('/update_floor/{floor_id}', [\App\Http\Controllers\FloorController::class, 'update']);
        Route::delete('/remove_floor/{floor_id}', [\App\Http\Controllers\FloorController::class, 'destroy']);
        /*Apartments*/
        Route::get('/building_apartments', [\App\Http\Controllers\ApartmentController::class, 'index']);
        Route::post('/new_apartment', [\App\Http\Controllers\ApartmentController::class, 'store']);
        /*Categories*/
        Route::get('/all_categories', [\App\Http\Controllers\CategoryController::class, 'all_categories']);
    });
    /*Admin endpoints*/
    Route::group(['middleware' => ['role:admin']], function () {
        /*Category*/
        Route::get('/categories', [\App\Http\Controllers\CategoryController::class, 'index']);
        Route::post('/category-create', [\App\Http\Controllers\CategoryController::class, 'store']);
        Route::put('/category-update/{category_id}', [\App\Http\Controllers\CategoryController::class, 'update']);
        Route::delete('/remove_category/{category_id}', [\App\Http\Controllers\CategoryController::class, 'destroy']);

        /*User*/
        Route::post('/create-role-permission', [\App\Http\Controllers\GeneralController::class, 'create_role_permission']);
        Route::get('/admin/users', [\App\Http\Controllers\AdminController::class, 'users']);
        Route::get('/admin/roles', [\App\Http\Controllers\AdminController::class, 'roles']);
        Route::post('/admin/update_role', [\App\Http\Controllers\AdminController::class, 'update_role']);
        Route::post('/admin/update_user_status', [\App\Http\Controllers\AdminController::class, 'update_user_status']);

    });

});


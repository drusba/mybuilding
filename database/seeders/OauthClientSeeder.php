<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OauthClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datetime = Carbon::now();
        $clients = [
            [
                'name' => 'Laravel Personal Access Client',
                'secret' => '1l04jM2tZI4KFVDoZu7uNCljmXQZqE2e7hcsnTcx',
                'personal_access_client' => 1,
                'password_client' => 0,
                'revoked' => 0,
                'redirect' => 'http://localhost/',
                'created_at' => $datetime,
                'updated_at' => $datetime
            ],
            [
                'name' => 'Laravel Password Grant Client',
                'secret' => 'AgKv9AZ0kGeDHtAtr97AF0kr1tMwAG0coxLft6jK',
                'personal_access_client' => 0,
                'password_client' => 1,
                'revoked' => 0,
                'redirect' => 'http://localhost/',
                'created_at' => $datetime,
                'updated_at' => $datetime
            ],
        ];

        DB::table('oauth_clients')->insert($clients);

    }
}

1.- git clone project
2.- composer install
3.- cp .env_to_review .env /***It has all the information to run the project (email user and app password, passport credential). Just update the credentials for the database.
4.- php artisan key:generate // gen app key
5.- php artisan migrate //generate all migrations
6.- php artisan db:seed //populate the database. if not work just do it individually 
        php artisan db:seed --class=OauthClientSeeder        
        php artisan db:seed --class=UserSeeder
        php artisan db:seed --class=RoleSeeder
        